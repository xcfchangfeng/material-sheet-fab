package com.gordonwong.materialsheetfab.sample.utils;

import com.gordonwong.materialsheetfab.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-03-18 14:46
 * description 自定义Toast
 */
public class MyToast {
    public static final int LENGTH_LONG = 4000;
    private ToastDialog toastDialog;
    private int offsetX, offsetY;

    public MyToast(int offsetX, int offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }


    public void createToast(Context mContext, String content, int duration, String bgColor) {
        Component itemView = LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_text, null, false);
        Text text = (Text) itemView.findComponentById(ResourceTable.Id_title);
        text.setText(content);
        if (bgColor.equals("#90ffffff")) {
            text.setTextColor(new Color(Color.getIntColor("#000000")));
        } else {
            text.setTextColor(new Color(Color.getIntColor("#ffffff")));
        }
        text.setPadding(vp2px(mContext, 15), vp2px(mContext, 8),
                vp2px(mContext, 15), vp2px(mContext, 8));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor(bgColor), vp2px(mContext, 5)));
        text.setMarginBottom(20);
        if (toastDialog != null) {
            toastDialog.cancel();
            toastDialog = null;
        }
        toastDialog = new ToastDialog(mContext);
        toastDialog.setComponent(itemView);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setTransparent(true);
        toastDialog.setDuration(duration);
        toastDialog.setOffset(offsetX, offsetY);
        toastDialog.show();
    }

    /**
     * toastIsShowing
     *
     * @return boolean toastIsShowing
     */
    public boolean toastIsShowing() {
        return toastDialog.isShowing();
    }

    private ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
