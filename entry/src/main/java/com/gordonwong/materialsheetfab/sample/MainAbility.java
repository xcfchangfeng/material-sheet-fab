package com.gordonwong.materialsheetfab.sample;

import com.gordonwong.materialsheetfab.sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        // 设置状态栏颜色
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(getColor(ResourceTable.Color_status_bar));
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
