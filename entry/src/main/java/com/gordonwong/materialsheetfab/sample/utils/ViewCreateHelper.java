package com.gordonwong.materialsheetfab.sample.utils;

import com.gordonwong.materialsheetfab.sample.ResourceTable;
import com.gordonwong.materialsheetfab.sample.models.Note;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-03-17 15:46
 * description ViewCreateHelper
 */
public final class ViewCreateHelper {
    private static final HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ViewCreateHelper");
    /**
     * 列表数据个数
     */
    private static final int NOTE_NUMBER_20 = 30;
    private static final int NOTE_NUMBER_7 = 17;
    private Context slice;
    private int scrollType = 0;

    public ViewCreateHelper(Context abilitySlice) {
        this.slice = abilitySlice;
    }

    public Component createView(String title, int position) {
        Component mainComponent;
        if (position == 2) {
            mainComponent =
                    LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_pager_item, null, false);
            if (!(mainComponent instanceof ComponentContainer)) {
                return null;
            }
            ScrollView scrollView = (ScrollView) mainComponent.findComponentById(ResourceTable.Id_refresh_layout);
            scrollView.enableScrollBar(1, true);
        } else {
            mainComponent =
                    LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_pager_layout_item, null, false);
            if (!(mainComponent instanceof ComponentContainer)) {
                return null;
            }
            ScrollView scrollView = (ScrollView) mainComponent.findComponentById(ResourceTable.Id_refresh_layout);
            scrollView.enableScrollBar(1, true);
        }
        initViewLayout(mainComponent, position);
        ComponentContainer rootLayout = (ComponentContainer) mainComponent;
        return rootLayout;
    }

    private void initViewLayout(Component mainComponent, int position) {
        DirectionalLayout rightLayout = null;
        if (position != 2) {
            rightLayout = (DirectionalLayout) mainComponent.findComponentById(ResourceTable.Id_right_layout);
        }
        DirectionalLayout leftLayout = (DirectionalLayout) mainComponent.findComponentById(ResourceTable.Id_left_layout);
        List<Note> notes;
        if (position == 0) {
            notes = generateNotes(slice, NOTE_NUMBER_20 / 2);
        } else {
            notes = generateNotes(slice, NOTE_NUMBER_7);
        }
        int leftTextSize = 0;
        int rightTextSize = 0;
        for (int i = 0; i < notes.size(); i++) {
            Component component =
                    LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_layout_text_item, null, false);
            Text titleTextView = (Text) component.findComponentById(ResourceTable.Id_note_title);
            Text noteTextView = (Text) component.findComponentById(ResourceTable.Id_note_text);
            DependentLayout infoLayout = (DependentLayout) component.findComponentById(ResourceTable.Id_note_info_layout);
            Text infoTextView = (Text) component.findComponentById(ResourceTable.Id_note_info);
            Image infoImageView = (Image) component.findComponentById(ResourceTable.Id_note_info_image);
            Note noteModel = notes.get(i);
            String title = noteModel.getTitle();
            String note = noteModel.getNote();
            String info = noteModel.getInfo();
            int infoImage = noteModel.getInfoImage();
            String color = noteModel.getColor();
            // Set text
            titleTextView.setText(title);
            noteTextView.setText(note);
            infoTextView.setText(info);
            // Set image
            if (infoImage != 0) {
                infoImageView.setPixelMap(infoImage);
            }

            // Set visibilities
            titleTextView.setVisibility(isEmpty(title) ? Component.HIDE : Component.VISIBLE);
            noteTextView.setVisibility(isEmpty(note) ? Component.HIDE : Component.VISIBLE);
            infoLayout.setVisibility(isEmpty(info) ? Component.HIDE : Component.VISIBLE);

            // Set padding
            int paddingTop = (titleTextView.getVisibility() != Component.VISIBLE) ? 0
                    : 16;
            noteTextView.setPadding(noteTextView.getPaddingLeft(), paddingTop,
                    noteTextView.getPaddingRight(), noteTextView.getPaddingBottom());
            // Set background color
            Element element = buildBg(Color.getIntColor(color), 0);
            component.setBackground(element);
            component.setTouchEventListener((component1, touchEvent) -> false);
            if (position != 2) {
                if (leftTextSize <= rightTextSize) {
                    leftTextSize = leftTextSize + note.length() + info.length() + title.length();
                    leftLayout.addComponent(component);
                } else {
                    rightLayout.addComponent(component);
                    rightTextSize = rightTextSize + note.length() + info.length() + title.length();
                }
            } else {
                leftLayout.addComponent(component);
            }
        }
    }

    private Element buildBg(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 生成数据
     *
     * @param context  Context
     * @param numNotes 列表展示的个数
     * @return 数据集合
     */
    private List<Note> generateNotes(Context context, int numNotes) {
        List<Note> notes = new ArrayList<>(numNotes);
        for (int i = 0; i < numNotes; i++) {
            notes.add(Note.randomNote(context));
        }
        return notes;
    }

    private boolean isEmpty(String content) {
        return content.isEmpty();
    }
}
