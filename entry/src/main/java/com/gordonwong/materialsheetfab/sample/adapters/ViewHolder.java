package com.gordonwong.materialsheetfab.sample.adapters;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.HashMap;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-03-17 18:13
 * description ViewHolder
 */
public class ViewHolder {
    public int position;
    public Text titleTextView;
    public Text noteTextView;
    public DependentLayout infoLayout;
    public Text infoTextView;
    public Image infoImageView;
    public Component component;
    private Context context;
    private HashMap<Integer, Component> views;

    ViewHolder(Context context, Component itemView, ComponentContainer parent, int position) {
        this.context = context;
        this.component = itemView;
        this.position = position;
        views = new HashMap<>(0);
        component.setTag(this);
    }

    @SuppressWarnings("unchecked")
    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = component.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }
}
