package com.gordonwong.materialsheetfab.sample.utils;

public class FastClickUtil {
    /**
     * 两次点击按钮之间的点击间隔不能少于1000毫秒
     */
    private static final int MIN_CLICK_DELAY_TIME = 650;
    private static long lastClickTime = -1;

    /**
     * 是否为快速点击
     *
     * @return 快速点击
     */
    public static boolean isFastClick() {
        boolean flag;
        long curClickTime = System.currentTimeMillis();
        if (curClickTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            flag = false;
        } else {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }
}
