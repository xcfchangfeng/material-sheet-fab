package com.gordonwong.materialsheetfab.sample.slice;

import com.gordonwong.materialsheetfab.animations.AnimationsProperty;
import com.gordonwong.materialsheetfab.animations.FabAnimation;
import com.gordonwong.materialsheetfab.animations.MaterialSheetAnimation;
import com.gordonwong.materialsheetfab.io.codetail.animation.ViewAnimationUtils;
import com.gordonwong.materialsheetfab.sample.ResourceTable;
import com.gordonwong.materialsheetfab.sample.adapters.MainPagerAdapter;
import com.gordonwong.materialsheetfab.sample.slide.OnDrawerCloseListener;
import com.gordonwong.materialsheetfab.sample.slide.OnDrawerOpenListener;
import com.gordonwong.materialsheetfab.sample.slide.OnDrawerScrollListener;
import com.gordonwong.materialsheetfab.sample.slide.SlidingDrawer;
import com.gordonwong.materialsheetfab.sample.utils.FastClickUtil;
import com.gordonwong.materialsheetfab.sample.utils.MyToast;
import com.gordonwong.materialsheetfab.sample.utils.ViewCreateHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;
import java.util.ArrayList;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-03-17 10:13
 * description 主界面
 */
public class MainAbilitySlice extends AbilitySlice implements TabList.TabSelectedListener,
        PageSlider.PageChangedListener, Component.ClickedListener, Animator.StateChangedListener {
    // 定义日志标签
    private TabList tabList;
    private PageSlider pageSlider;
    private int pageSlideSelect = 0;
    /**
     * 蒙层
     */
    private DirectionalLayout transparencyLayout, refreshLayout, directionalLayout_zt;

    /**
     * 添加动画效果
     */
    private AnimatorProperty mAnimatorIcon;
    private AnimatorProperty mAnimatorLayout;
    /**
     * 点击展示侧边菜单
     */
    private DependentLayout leftImage, rightImage, searchImg;
    /**
     * 圆形按钮
     */
    private DependentLayout dependentLayout;

    private AnimatorValue animation, animatorValue;
    private StackLayout mStackLayout;
    private Component mImage;
    private DirectionalLayout myDirectionalLayout;
    // onTouchEvent事件中记录down时的Y轴值
    private Image mImageZZ;
    DependentLayout mMainView;
    boolean issy = false;
    boolean isxy = true;
    boolean isFd = false;
    boolean isSx = false;
    /**
     * 侧边栏是否是拖动出来的
     */
    private boolean slidingIsDrawer = false;
    TaskDispatcher globalTaskDispatcher;
    AnimatorProperty animatorProperty6;
    boolean isTouch = false;
    private Display display;
    private MyToast myToast;
    DirectionalLayout listcr, listrd, listrm, listPhoto;
    private DirectionalLayout mNavigationLayout;
    private Component mMaskingLayout;
    private AnimatorProperty mAnimatorLayoutCL;
    private DirectionalLayout mNavTitleLayout;
    private float mTouchStartX;
    private float mContentPositionX;
    private float mContentPositionY;
    private float mMoveX;
    private AnimatorValue mAnimatorValue;
    private float mLayoutMoveX;
    private int mAnimatorEndFlag = -1;
    private static final long TIMER_DRAWER_ANIMATOR = 200;
    private MmiPoint point;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        globalTaskDispatcher = getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        initView();
        initData();
    }

    float lastX, lastY;

    float ContentPositionY = 0;

    private void initView() {
        display = DisplayManager.getInstance().getDefaultDisplay(this).get();
        mMaskingLayout = findComponentById(ResourceTable.Id_masking_layout);
        mMaskingLayout.setClickedListener(this);

        mNavigationLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_main_drawer_layout);
        searchImg = (DependentLayout) findComponentById(ResourceTable.Id_Search_img);
        mMainView = (DependentLayout) findComponentById(ResourceTable.Id_Main_view);
        mStackLayout = (StackLayout) findComponentById(ResourceTable.Id_ST_Layout);
        mImageZZ = (Image) findComponentById(ResourceTable.Id_Image_zz);
        myDirectionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_Md_Layout);
        mImage = findComponentById(ResourceTable.Id_bg);
        myDirectionalLayout.setClickedListener(this);
        refreshLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_refresh);
        directionalLayout_zt = (DirectionalLayout) findComponentById(ResourceTable.Id_DirectionalLayout_zt);
        rightImage = (DependentLayout) findComponentById(ResourceTable.Id_right_icon);
        transparencyLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_transparency);
        transparencyLayout.setClickedListener(this);
        dependentLayout = (DependentLayout) findComponentById(ResourceTable.Id_circleBtn);
        dependentLayout.setClickedListener(this);
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pager);
        tabList.addTabSelectedListener(this);
        pageSlider.addPageChangedListener(this);
        tabList.setFixedMode(true);
        leftImage = (DependentLayout) findComponentById(ResourceTable.Id_left_icon);
        leftImage.setClickedListener(this);

        listcr = (DirectionalLayout) findComponentById(ResourceTable.Id_list_cr);
        listrd = (DirectionalLayout) findComponentById(ResourceTable.Id_list_rd);
        listrm = (DirectionalLayout) findComponentById(ResourceTable.Id_list_rm);
        listPhoto = (DirectionalLayout) findComponentById(ResourceTable.Id_list_photo);
        listcr.setClickedListener(this);
        listrd.setClickedListener(this);
        listrm.setClickedListener(this);
        listPhoto.setClickedListener(this);

        initListeners();

        AnimatorProperty animatorProperty5 = mImageZZ.createAnimatorProperty();
        animatorProperty5.scaleX(10).scaleY(10).alpha(0).setDuration(800);

        animatorProperty6 = mImageZZ.createAnimatorProperty();
        animatorProperty6.scaleX(-2).scaleY(-2).alpha(90).setDelay(600).setDuration(1000);

        dependentLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (FastClickUtil.isFastClick()) {
                    return;
                }
                pageSlider.setEnabled(false);
                pageSlider.setClickable(false);
                isTouch = true;
                mImageZZ.setVisibility(Component.VISIBLE);
                animatorProperty5.start();
                AnimatorProperty animatorProperty = ViewAnimationUtils.menuMoveUp(dependentLayout, -120, -70, 0, 200);
                animatorProperty.start();
                //放大比例
                AnimatorProperty enLargeProperty = ViewAnimationUtils.layoutEnlarge(mStackLayout, 4, -120, 300);
                enLargeProperty.start();
                mImage.setVisibility(Component.VISIBLE);
                myDirectionalLayout.setVisibility(Component.VISIBLE);
            }
        });
        // 动效
        animatorValue = new AnimatorValue();
        animatorValue.setDuration(500);
        animatorValue.setCurveType(7);

        optionMoreLayout();
        animation = new AnimatorValue();
        animation.setDuration(500);
        animation.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                dependentLayout.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                // notify the listener
                if (pageSlideSelect == 2) {
                    dependentLayout.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
    }

    private void initListeners() {
        mMaskingLayout.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_UP:
                        executeAnimation(1);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        mNavigationLayout.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        pageSlider.setEnabled(false);
                        mTouchStartX = touchEvent.getPointerScreenPosition(0).getX();
                        mContentPositionX = component.getContentPositionX();
                        mContentPositionY = component.getContentPositionY();
                        break;
                    case TouchEvent.POINT_MOVE:
                        if (mMaskingLayout.getVisibility() == Component.HIDE) {
                            mMaskingLayout.setVisibility(Component.VISIBLE);
                        }
                        mMoveX = touchEvent.getPointerScreenPosition(0).getX() - mTouchStartX;
                        if (mContentPositionX + mMoveX < -700 && mLayoutMoveX > -700) {
                            component.setContentPosition(-700, (int) mContentPositionY);
                            mMaskingLayout.setAlpha(0);
                            break;
                        }
                        if (mContentPositionX + mMoveX > 0 && mLayoutMoveX < 0) {
                            component.setContentPosition(0, (int) mContentPositionY);
                            mMaskingLayout.setAlpha(0.5f);
                            break;
                        }
                        mLayoutMoveX = mContentPositionX + mMoveX;
                        if (mLayoutMoveX <= 0 && mLayoutMoveX >= -700) {
                            component.setContentPosition((int) mLayoutMoveX, (int) mContentPositionY);
                            mMaskingLayout.setAlpha((1 + mLayoutMoveX / 700) / 2);
                        }
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                    case TouchEvent.CANCEL:
                        float upX = mContentPositionX + mMoveX;
                        if (upX <= 0 && upX > -700) {
                            mAnimatorLayoutCL = component.createAnimatorProperty();
                            mAnimatorValue = new AnimatorValue();
                            mAnimatorValue.setDuration(TIMER_DRAWER_ANIMATOR);
                            mAnimatorValue.setValueUpdateListener((animatorValue, v) -> {
                                if (upX > -350 && mMoveX > 0) {
                                    mMaskingLayout.setAlpha((1 + upX * (1 - v) / 700) / 2);
                                    mAnimatorEndFlag = 0;
                                } else if (upX <= -350 && mMoveX > 0) {
                                    mMaskingLayout.setAlpha((1 + (upX + (-700 - upX) * v) / 700) / 2);
                                    mAnimatorEndFlag = 1;
                                } else if (upX > -100 && mMoveX <= 0) {
                                    mMaskingLayout.setAlpha((1 + upX * (1 - v) / 700) / 2);
                                    mAnimatorEndFlag = 0;
                                } else if (upX <= -100 && mMoveX <= 0) {
                                    mMaskingLayout.setAlpha((1 + (upX + (-700 - upX) * v) / 700) / 2);
                                    mAnimatorEndFlag = 1;
                                }
                            });
                            mAnimatorValue.setStateChangedListener(MainAbilitySlice.this);
                            mAnimatorValue.start();
                            if (upX > -350 && mMoveX > 0) {
                                mAnimatorLayoutCL.moveFromX(upX).moveToX(0).setDuration(TIMER_DRAWER_ANIMATOR);
                            } else if (upX <= -350 && mMoveX > 0) {
                                mAnimatorLayoutCL.moveFromX(upX).moveToX(-700).setDuration(TIMER_DRAWER_ANIMATOR);
                            } else if (upX > -100 && mMoveX <= 0) {
                                mAnimatorLayoutCL.moveFromX(upX).moveToX(0).setDuration(TIMER_DRAWER_ANIMATOR);
                            } else if (upX <= -100 && mMoveX <= 0) {
                                mAnimatorLayoutCL.moveFromX(upX).moveToX(-700).setDuration(TIMER_DRAWER_ANIMATOR);
                            }
                            mAnimatorLayoutCL.start();
                        }

                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        leftImage.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_UP:
                        rotateAnimator();
                        executeAnimation(0);
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        searchImg.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

            }
        });
        searchImg.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                MyToast toast = new MyToast(display.getAttributes().width - vp2px(component.getContext(), 250),
                        display.getAttributes().height - (int) component.getContentPositionY() - vp2px(component.getContext(), 100));
                toast.createToast(component.getContext(), "Search", MyToast.LENGTH_LONG, "#70000000");
            }
        });
        mImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (FastClickUtil.isFastClick()) {
                    return;
                }
                isTouch = false;
                pageSlider.setEnabled(true);
                pageSlider.setClickable(true);
                mImage.setVisibility(Component.HIDE);
                //圆圈下移
                AnimatorProperty animatorProperty = ViewAnimationUtils.menuMoveDown(dependentLayout, 120, 70, 90, 200);
                animatorProperty.start();
                //布局缩小
                AnimatorProperty animatorProperty2 = ViewAnimationUtils.layoutReduce(mStackLayout, 120, 300);
                animatorProperty2.start();
                //遮罩缩小
                animatorProperty6.start();
            }
        });
        mImage.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });
        pageSlider.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                if (!pageSlider.isEnabled()) {
                    return true;
                }
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        refreshLayout.setVisibility(Component.HIDE);
                        refreshLayout.setWidth(0);
                        point = touchEvent.getPointerPosition(touchEvent.getIndex());
                        if (height == 0) {
                            height = pageSlider.getHeight();
                        }
                        lastX = getTouchX(touchEvent, 0, component);
                        lastY = getTouchY(touchEvent, 0, component);
                        return true;
                    case TouchEvent.POINT_MOVE:
                        refreshLayout.setVisibility(Component.HIDE);
                        refreshLayout.setWidth(0);
                        System.out.println(point.getX());
                        float nowY = getTouchY(touchEvent, 0, component);
                        if (Math.abs(nowY - lastY) > 10) {
                            ContentPositionY += nowY - lastY;
                            if (nowY > lastY) {
                                dropDown();
                            } else {
                                slide();
                            }
                        }
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                    case TouchEvent.CANCEL:
                        break;
                }
                return false;
            }
        });
    }

    private float height;

    public void dropDown() {
        if (ContentPositionY >= 0) {
            ContentPositionY = 0;
        }
        directionalLayout_zt.setTranslationY(ContentPositionY);
    }

    public void slide() {
        if (ContentPositionY < -150) {
            ContentPositionY = -150;
        }
        directionalLayout_zt.setTranslationY(ContentPositionY);
    }

    /**
     * 设置tabList
     */
    private void initData() {
        try {
            String[] tabsTitle = getResourceManager().getElement(ResourceTable.Strarray_tab_title).getStringArray();
            for (String title : tabsTitle) {
                TabList.Tab tab = tabList.new Tab(getContext());
                tab.setText(title);
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(new RgbColor(0, 0, 0, 0));
                tab.setBackground(shapeElement);
                tabList.addTab(tab);
            }
            tabList.selectTabAt(0);
            pageSlider.setProvider(new MainPagerAdapter(initPageSliderViewData()));
            pageSlider.setCurrentPage(0);
            pageSlider.setReboundEffect(false);
            pageSlider.setCentralScrollMode(true);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSliding(int itemPos, float itemPosOffset, int itemPosPixles) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
        if (isTouch) {
            pageSlider.setEnabled(false);
            pageSlider.setClickable(false);
        }
    }

    @Override
    public void onPageChosen(int pageSlideSelect) {
        if (isTouch) {
            return;
        }
        if (tabList.getSelectedTab().getPosition() != pageSlideSelect) {
            tabList.selectTabAt(pageSlideSelect);
        }
        if (pageSlideSelect == 0) {
            mMainView.setVisibility(Component.HIDE);
            if (issy) {
                issy = false;
                isxy = true;
                isSx = false;
                isFd = true;
                MaterialSheetAnimation.morphFromSheet(1, 1, 500, dependentLayout);
                AnimationsProperty.morphFromSheet(0, 150, 200, dependentLayout);
            }
            if (isxy) {
                issy = false;
                isxy = true;
                isSx = false;
                isFd = true;
                MaterialSheetAnimation.morphFromSheet(1, 1, 500, dependentLayout);
            }
        } else if (pageSlideSelect == 1) {

            mMainView.setVisibility(Component.VISIBLE);
            if (isxy) {
                issy = true;
                isxy = false;
                isSx = false;
                isFd = true;
                FabAnimation.morphFromSheet(0, -150, 200, dependentLayout);
            }
            if (issy) {
                issy = true;
                isxy = false;
                isSx = false;
                isFd = true;
                MaterialSheetAnimation.morphFromSheet(1, 1, 500, dependentLayout);
            }
        } else if (pageSlideSelect == 2) {
            mMainView.setVisibility(Component.HIDE);
            if (issy) {
                issy = true;
                isxy = false;
                isSx = true;
                isFd = false;
                MaterialSheetAnimation.morphFromSheet(0, 0, 500, dependentLayout);
            }
            if (isxy) {
                issy = false;
                isxy = true;
                isSx = true;
                isFd = false;
                MaterialSheetAnimation.morphFromSheet(0, 0, 500, dependentLayout);
            }
        }
    }

    //数据
    private ArrayList<Component> initPageSliderViewData() {
        int pageSize = tabList.getTabCount();
        ArrayList<Component> pages = new ArrayList<>();
        ViewCreateHelper viewCreateHelper = new ViewCreateHelper(getContext());
        for (int i = 0; i < pageSize; i++) {
            pages.add(viewCreateHelper.createView(tabList.getTabAt(i).getText(), i));
        }
        return pages;
    }

    @Override
    public void onSelected(TabList.Tab tab) {
        if (isTouch) {
            if (pageSlider.getCurrentPage() != tab.getPosition()) {
                pageSlider.setCurrentPage(0);
                tabList.selectTabAt(0);
            }
        } else {
            if (pageSlider.getCurrentPage() != tab.getPosition()) {
                pageSlider.setCurrentPage(tab.getPosition());
            }
        }
    }

    @Override
    public void onUnselected(TabList.Tab tab) {

    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (mAnimatorIcon != null) {
            mAnimatorIcon = null;
        }
        if (mAnimatorLayout != null) {
            mAnimatorLayout = null;
        }
    }

    @Override
    public void onReselected(TabList.Tab tab) {

    }

    @Override
    public void onClick(Component c) {
        switch (c.getId()) {
            case ResourceTable.Id_left_icon:
                executeAnimation(0);
                rotateAnimator();
                break;
            case ResourceTable.Id_list_rd:
            case ResourceTable.Id_list_rm:
            case ResourceTable.Id_list_cr:
            case ResourceTable.Id_list_photo:
                if (FastClickUtil.isFastClick()) {
                    return;
                }
                isTouch = false;
                pageSlider.setEnabled(true);
                pageSlider.setClickable(true);
                mImage.setVisibility(Component.HIDE);
                //圆圈下移
                AnimatorProperty animatorProperty = ViewAnimationUtils.menuMoveDown(dependentLayout, 120, 70, 90, 200);
                animatorProperty.start();
                //布局缩小
                AnimatorProperty animatorProperty2 = ViewAnimationUtils.layoutReduce(mStackLayout, 120, 300);
                animatorProperty2.start();
                //遮罩缩小
                animatorProperty6.start();
                initDialogShow();
                break;
            default:
                break;
        }
    }

    private void initDialogShow() {
        MyToast toast = new MyToast(0, vp2px(getContext(), 10));
        toast.createToast(this, "Item pressed", MyToast.LENGTH_LONG, "#90ffffff");
    }

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {
        if (mAnimatorEndFlag == 1) {
            pageSlider.setEnabled(true);
            mMaskingLayout.setVisibility(Component.HIDE);
        }
    }

    @Override
    public void onPause(Animator animator) {
    }

    @Override
    public void onResume(Animator animator) {

    }

    /**
     * 选择要执行的动画
     *
     * @param whichAni
     */
    private void executeAnimation(int whichAni) {
        mAnimatorValue = new AnimatorValue();
        mAnimatorValue.setDuration(TIMER_DRAWER_ANIMATOR);
        if (whichAni == 0) {
            pageSlider.setEnabled(false);
            if (mNavigationLayout.getContentPositionX() < (-700 + 50)) {
                mAnimatorValue.setValueUpdateListener((animatorValue, v) -> {
                    mNavigationLayout.setContentPosition((int) (-700 * (1 - v)), (int) mNavigationLayout.getContentPositionY());
                    mMaskingLayout.setVisibility(Component.VISIBLE);
                    mMaskingLayout.setAlpha(v / 2);
                    mAnimatorEndFlag = 0;
                });
            }
        } else if (whichAni == 1) {
            if (mNavigationLayout.getContentPositionX() > -50) {
                mAnimatorValue.setValueUpdateListener((animatorValue, v) -> {
                    mNavigationLayout.setContentPosition((int) (-700 * v), (int) mNavigationLayout.getContentPositionY());
                    mMaskingLayout.setAlpha((1 - v) / 2);
                    mAnimatorEndFlag = 1;
                });
            }
        }
        mAnimatorValue.setStateChangedListener(this);
        mAnimatorValue.start();
    }

    private void rotateAnimator() {
        if (null != mAnimatorIcon) {
            mAnimatorIcon.reset();
        }
        mAnimatorIcon = leftImage.createAnimatorProperty();
        mAnimatorIcon.rotate(180).setDuration(200);
        //开启动画
        mAnimatorIcon.start();
    }

    private int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }

    private void optionMoreLayout() {
        refreshLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                refreshLayout.setVisibility(Component.HIDE);
                refreshLayout.setWidth(0);
            }
        });
        refreshLayout.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });

        animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                refreshLayout.setWidth(vp2px(dependentLayout.getContext(), v * 150));
                refreshLayout.setVisibility(Component.VISIBLE);
            }
        });
        rightImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (myToast == null || !myToast.toastIsShowing()) {
                    animatorValue.start();
                }
            }
        });

        rightImage.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                myToast = new MyToast(display.getAttributes().width - vp2px(component.getContext(), 250),
                        display.getAttributes().height - (int) component.getContentPositionY() - vp2px(component.getContext(), 100));
                myToast.createToast(component.getContext(), "More options", MyToast.LENGTH_LONG, "#80000000");
            }
        });
    }

    private float getTouchX(TouchEvent event, int index, Component component) {
        float x = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                x = event.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                x = event.getPointerPosition(index).getX();
            }
        }
        return x;
    }

    private float getTouchY(TouchEvent event, int index, Component component) {
        float y = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                y = event.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                y = event.getPointerPosition(index).getY();
            }
        }
        return y;
    }

}
