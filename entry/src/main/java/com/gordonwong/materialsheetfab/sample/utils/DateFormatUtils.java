package com.gordonwong.materialsheetfab.sample.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-03-18 11:01
 * description 格式化时间
 */
public class DateFormatUtils {
    /**
     * 格式化日期 May 12, 2021
     *
     * @param date 日期
     * @return 日期
     */
    public static String dateFormat(Date date) {
        Calendar cale = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",
                Locale.ENGLISH);
        return sdf.format(cale.getTime());
    }
}
