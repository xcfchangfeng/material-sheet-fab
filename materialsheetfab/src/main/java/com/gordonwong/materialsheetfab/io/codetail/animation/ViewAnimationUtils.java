/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gordonwong.materialsheetfab.io.codetail.animation;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.StackLayout;

/**
 * author zhaoxudong
 * Version 1.0
 * ModifiedBy
 * date 2021-05-26 16:24
 * description
 */
public class ViewAnimationUtils {
    /**
     * 悬浮按钮上移
     *
     * @param dependentLayout DependentLayout
     * @param moveByX         moveByX
     * @param moveByY         moveByY
     * @param alpha           透明度
     * @param duration        动画时长
     * @return AnimatorProperty
     */
    public static AnimatorProperty menuMoveUp(DependentLayout dependentLayout, int moveByX, int moveByY, int alpha, int duration) {
        //圆圈上移
        AnimatorProperty animatorProperty = dependentLayout.createAnimatorProperty();
        animatorProperty.moveByX(moveByX).moveByY(moveByY).rotate(0).alpha(alpha).setDuration(duration);
        return animatorProperty;
    }

    /**
     * 悬浮按钮下移
     *
     * @param dependentLayout DependentLayout
     * @param moveByX         moveByX
     * @param moveByY         moveByY
     * @param alpha           透明度
     * @param duration        动画时长
     * @return AnimatorProperty
     */
    public static AnimatorProperty menuMoveDown(DependentLayout dependentLayout, int moveByX, int moveByY, int alpha, int duration) {
        AnimatorProperty animatorProperty = dependentLayout.createAnimatorProperty();
        animatorProperty.moveByX(moveByX).moveByY(moveByY).setDelay(600).alpha(alpha).setDuration(duration);
        return animatorProperty;
    }

    /**
     * 布局缩小
     *
     * @param stackLayout StackLayout
     * @param moveByY     moveByY
     * @param duration    动画时长
     * @return AnimatorProperty
     */
    public static AnimatorProperty layoutReduce(StackLayout stackLayout, int moveByY, int duration) {
        AnimatorProperty animatorProperty = stackLayout.createAnimatorProperty();
        animatorProperty.scaleX(0).scaleY(0).moveByY(moveByY).setDelay(500).alphaFrom(0).setDuration(duration);
        return animatorProperty;
    }

    /**
     * 布局放大
     *
     * @param stackLayout StackLayout
     * @param scale       缩放
     * @param moveByY     moveByY
     * @param duration    动画时长
     * @return AnimatorProperty
     */
    public static AnimatorProperty layoutEnlarge(StackLayout stackLayout, int scale, int moveByY, int duration) {
        AnimatorProperty animatorProperty = stackLayout.createAnimatorProperty();
        animatorProperty.scaleX(scale).scaleY(scale).moveByY(moveByY).setDelay(200).alpha(90).setDuration(duration);
        return animatorProperty;
    }
}
