package com.gordonwong.materialsheetfab.animations;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.DependentLayout;

public class FabAnimation {
    public static void morphFromSheet(int x, int y, int duration, DependentLayout dependentLayout) {
        //yd
        AnimatorProperty animatorProperty3 = dependentLayout.createAnimatorProperty();
        morph(x, y, duration, animatorProperty3);
    }

    protected static void morph(int x, int y, int duration, AnimatorProperty animatorProperty) {
        animatorProperty.moveByY(y).setDuration(duration);
        startArcAnim(animatorProperty);
    }

    protected static void startArcAnim(AnimatorProperty animatorProperty) {
        animatorProperty.start();
    }
}
