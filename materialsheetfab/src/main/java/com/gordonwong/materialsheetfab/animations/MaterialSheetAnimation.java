package com.gordonwong.materialsheetfab.animations;

import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.DependentLayout;

public class MaterialSheetAnimation {
    public static void morphFromSheet(int x, int y, int duration, DependentLayout dependentLayout) {
        //放大缩小比例
        AnimatorProperty animatorProperty3 = dependentLayout.createAnimatorProperty();
        morph(x, y, duration, animatorProperty3);
    }

    protected static void morph(int x, int y, int duration, AnimatorProperty animatorProperty) {
        animatorProperty.scaleX(x).scaleY(y).setDuration(duration);
        startArcAnim(animatorProperty);
    }

    protected static void startArcAnim(AnimatorProperty animatorProperty) {
        animatorProperty.start();
    }
}
