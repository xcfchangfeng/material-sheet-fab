# material-sheet-fab

#### 项目介绍
- 项目名称：浮动操作按钮
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现浮动操作按钮到工作表的转换
- 项目移植状态：主功能完成
- 调用差异：通过自定义接口对外提供调用
- 开发版本：sdk5，DevEco Studio2.1 Release
- 基线版本：Release v1.2.1

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:material-sheet-fab:0.0.1-SNAPSHOT')
    ......  
 }
```

在sdk5，DevEco Studio 2.1 Release下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

```java
AnimatorProperty animatorProperty = ViewAnimationUtils.menuMoveUp(dependentLayout, -120, -70, 0, 200);
                animatorProperty.start();
                //放大比例
AnimatorProperty enLargeProperty = ViewAnimationUtils.layoutEnlarge(mStackLayout, 4, -120, 300);
                enLargeProperty.start();

MaterialSheetAnimation.morphFromSheet(1, 1, 500, dependentLayout);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

火绒安全病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT

#### 版权和许可信息

    The MIT License (MIT)
    
    Copyright (c) 2015 Gordon Wong
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
